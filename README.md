# holochain minimal templates

Minimalistic templates for the [scaffolding tool](https://github.com/holochain/scaffolding), to develop apps and modules using the holochain-open-dev style!

Available templates
- `headless`: a template without any ui project


## Using the templates

> WARNING: these templates are only compatible with holochain v0.2.x.

1a. If you are scaffolding a new project, run this:

`nix run --override-input versions 'github:holochain/holochain?dir=versions/0_2' github:holochain/holochain#hc-scaffold -- web-app --templates-url https://github.com/holochain-open-dev/templates`

1b. If you already have an existing project, run this inside a `nix develop` shell:

`hc scaffold template get https://github.com/holochain-open-dev/templates`
